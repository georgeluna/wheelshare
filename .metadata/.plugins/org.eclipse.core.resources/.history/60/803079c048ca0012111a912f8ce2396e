package com.wheelshare.objects;

import java.lang.reflect.Type;
import java.util.List;
import android.app.Activity;
import android.util.Log;
import com.wheelshare.gaeinterface.*;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang.ArrayUtils;

//http://www.mkyong.com/java/how-do-convert-java-object-to-from-json-format-gson-api/
//http://stackoverflow.com/questions/450807/java-generics-how-do-i-make-the-method-return-type-generic

public class ObjectManager {
	public static User currentUser; //should remain constant through all instances of this class
	
	public static Activity currentActivity = null;
	
	/**IMPORTANT: this must be executed before each time that
	 * UserManager is set within an activity class.
	 */
	public static void setCurrentActivity(Activity activity)
	{
		currentActivity = activity;
	}
	
	/**Convers ints in the format of YYYYMMDD into strings*/
	public static String getDate(int date)
	{
		int year = date / 10000;
		int month = (date % 10000)/100;
		int day = date % 100;
		String str = "";
		if(month == 1) str += "January ";
		else if(month == 2) str += "February ";
		else if(month == 3) str += "March ";
		else if(month == 4) str += "April ";
		else if(month == 5) str += "May ";
		else if(month == 6) str += "June ";
		else if(month == 7) str += "July ";
		else if(month == 8) str += "August ";
		else if(month == 9) str += "September ";
		else if(month == 10) str += "October ";
		else if(month == 11) str += "November ";
		else if(month == 12) str += "December ";
		str += String.valueOf(day);
		if(day/10 == 1) str += "th, ";
		else
		{
			int singleDay = day % 10;
			if(singleDay == 1) str += "st, ";
			else if(singleDay == 2) str += "nd, ";
			else if(singleDay == 3) str += "rd, ";
			else str += "th, ";
		}
		str += String.valueOf(year);
		return str;
	}
	
	public static String getTime(int time)
	{
		boolean pm = false;
		int hour = 0;
		int minute = 0;
		String str = "";
		if(time > 1159)
		{
			pm = true;
			time -= 1200;
		}
		hour = time / 100;
		minute = time % 100;
		if(hour == 0) hour = 12;
		str += String.valueOf(hour);
		if(minute < 10) str += ":0" + String.valueOf(minute);
		else str += ":" + String.valueOf(minute);
		if(pm) str += "PM";
		else str += "AM";
		return str;
	}
	
	public static <T extends DataObject> Type getObjectListType(T requestObject)
	{
		if(requestObject instanceof User) return new TypeToken<List<User>>(){}.getType();
		else if(requestObject instanceof Ride) return new TypeToken<List<Ride>>(){}.getType();
		else if(requestObject instanceof Trip) return new TypeToken<List<Trip>>(){}.getType();
		else if(requestObject instanceof Passenger) return new TypeToken<List<Passenger>>(){}.getType();
		else if(requestObject instanceof Rating) return new TypeToken<List<Rating>>(){}.getType();
		return new TypeToken<List<DataObject>>(){}.getType();
	}
	
	public static <T extends DataObject> Type getObjectType(T requestObject)
	{
		if(requestObject instanceof User) return new TypeToken<User>(){}.getType();
		else if(requestObject instanceof Ride) return new TypeToken<Ride>(){}.getType();
		else if(requestObject instanceof Trip) return new TypeToken<Trip>(){}.getType();
		else if(requestObject instanceof Passenger) return new TypeToken<Passenger>(){}.getType();
		else if(requestObject instanceof Rating) return new TypeToken<Rating>(){}.getType();
		return new TypeToken<DataObject>(){}.getType();
	}
	
	public static <T extends DataObject> String getUriLocation(T uriObject)
	{
		if(uriObject instanceof User) return "users";
		else if(uriObject instanceof Ride) return "rides";
		else if(uriObject instanceof Trip) return "trips";
		else if(uriObject instanceof Passenger) return "passengers";
		else if(uriObject instanceof Rating) return "ratings";
		return "";
	}
	
	/**This function returns a JSON string based on what the server
	 * returns given a request and parameters stored within the given
	 * object. "GET" methods are used for querying objects, "POST"
	 * methods are used for creating new objects, and so on.
	 */
	private static <T extends DataObject> String makeRequest(String method, String uriLocation, T requestObject)
	{
		String uriString = "http://wheelshare160.appspot.com/" + uriLocation;
		String [] params = {method, uriString};
		String [] finalParams = (String[]) ArrayUtils.addAll(params, requestObject.toStringArray());
		return new AppEngineInterface().makeRequest(finalParams);
	}
    
	public static <T extends DataObject> List<T> search(T queryObject)
	{		
		Gson gson = new Gson();
		List<T> dataobjects = gson.fromJson(makeRequest("GET",getUriLocation(queryObject),queryObject),getObjectListType(queryObject));
		return dataobjects;
	}
	
	/**Adds new object to the Datastore. Note that it would probably
	 * be a good idea to have this function return NULL if the
	 * object already exists in the datastore. Let's just assume
	 * that it does for now.
	 */
	public static <T extends DataObject> T add(T newObject)
	{
		Gson gson = new Gson();
		String str = makeRequest("POST",getUriLocation(newObject),newObject);
		//Log.d("wheelshare160",str);
		T returnObject = gson.fromJson(str,getObjectType(newObject));
		Log.i("wheelshare160","New object created in " + getUriLocation(newObject) + ": " + returnObject.toString());
		/**After a successful object creation, the current object is set
		 * to the newly created object that is sent back from the server.
		 */
		return returnObject;
	}
	
	/**Instead of doing a POST, this function performs a PUT
	 * so that the current object is updated in the database.
	 */
	public static <T extends DataObject> T update(T updatedObject)
	{
		Gson gson = new Gson();
		T returnObject = gson.fromJson(makeRequest("PUT",getUriLocation(updatedObject),updatedObject),getObjectType(updatedObject));
		Log.i("wheelshare160","New object created in " + getUriLocation(updatedObject) + ": " + returnObject.toString());
		/**After a successful object update, the current object is set
		 * to the newly created object that is sent back from the server.
		 */
		return returnObject;
	}
	
	/**Instead of doing a POST, this function performs a DELETE
	 * so that the current object is deleted from the database.
	 * @return 
	 */
	public <T extends DataObject> void delete(T deletedObject)
	{
		Gson gson = new Gson();
		T returnObject = gson.fromJson(makeRequest("DELETE",getUriLocation(deletedObject),deletedObject),getObjectType(deletedObject));
		Log.i("wheelshare160","Object deleted: " + returnObject.toString());
		//TODO IMPLEMENT THIS IN THE APP ENGINE INTERFACE!
	}
	
	/**Registers a new user. This function returns 0 if the user does not provide a
	 * username, -1 if no password is provided, -2 if no email is provided, -3 if a user
	 * with the same username already exists, -4 if a user with the same email already
	 * exists, and finally a 1 if the user creation was successful.
	 */
	public static int registerUser(User newUser, String verifyPassword)
	{
		if(newUser.getUserName() == null || newUser.getUserName().equals(""))
		{
			Log.e("wheelshare160","ERROR: Username field was left blank.");
			return 0;
		}
		if(newUser.getPassword() == null || newUser.getPassword().equals(""))
		{
			Log.e("wheelshare160","ERROR: Password field was left blank.");
			return -1;
		}
		if(!newUser.getPassword().equals(verifyPassword))
		{
			Log.e("wheelshare160","ERROR: Passwords do not match.");
			return -5;
		}
		if(newUser.getEmail() == null || newUser.getEmail().equals(""))
		{
			Log.e("wheelshare160","ERROR: Email field was left blank.");
			return -2;
		}
		
		User queryUsername = new User();
		queryUsername.setUserName(newUser.getUserName());
		List<User> userIDResults = search(queryUsername);
		if(userIDResults.size() != 0)
		{
			Log.e("wheelshare160","ERROR: User \"" + newUser.getUserName() + "\" already exists. Please choose a different username.");
			return -3;
		}
		
		User queryEmail = new User();
		queryEmail.setEmail(newUser.getEmail());
		List<User> emailResults = search(queryEmail);
		if(emailResults.size() != 0)
		{
			Log.e("wheelshare160","ERROR: The email \"" + newUser.getEmail() + "\" is already registered within the system. Please use a different email address.");
			return -4;
		}
		
		/**once the inputted user has passed all of the above tests, we finally set the
		 * current user to the user that we want to add.
		 */
		//currentUser = addUser(newUser);
		currentUser = add(newUser);
		Log.i("wheelshare160","User \""+currentUser.getUserName()+"\" was successfully registered.");
		return 1;
	}
	
	/**This function performs some simple verification. If the user
	 * and password match, then it will return a 1. If they don't,
	 * then it will return a 0. If the user does not exist, then it
	 * will return a -1.
	 */
	public static int verifyUser(String username, String password)
	{
		if(username == null || password == null || username.equals("") || password.equals(""))
		{
			Log.e("wheelshare160","Can't leave Username and/or Password field blank!");
			return -2; //can't leave username or password blank
		}
		User queryUser = new User();
		queryUser.setUserName(username);
		List<User> results = search(queryUser);
		if(results.size() != 0)
		{
			if((results.get(0)).getPassword().equals(password))
			{
				currentUser = results.get(0);
				Log.i("wheelshare160","Login successful, now logged in as " + currentUser.getUserName());
				return 1; //verified
			}
			else
			{
				Log.e("wheelshare160","Incorrect Password.");
				return 0; //login failed because of wrong password
			}
		}
		Log.e("wheelshare160","User does not exist.");
		return -1; //login failed because user does not exist
	}
	/**Returns the average rating for a particular user*/
	public static float getUserRating(int userID)
	{
		Rating userRatingQuery = new Rating();
		//List<Rating> ratings = searchRating(userRatingQuery);
		List<Rating> ratings = search(userRatingQuery);
		float averageRating = 0;
		for(Rating rating : ratings) averageRating += (float) rating.getRating();
		return averageRating / (float) ratings.size();
	}
	
	/**This sets the current user to null, which is necessary when logging out*/
	public static void logOut()
	{
		currentUser = null;
	}
	
}