#http://www.youtube.com/watch?v=GhgvBOsJROQ
import datetime
import cgi
import urllib
import webapp2
import json
import logging

from google.appengine.ext import db
from google.appengine.api import users

class User(db.Model):
	#REMEMBER TO CONVERT STRINGS TO intS/INTS WHEN YOU ARE WRITING THE REQUEST CLASSES!
	userName = db.StringProperty()
	gender = db.IntegerProperty()
	email = db.StringProperty()
	firstName = db.StringProperty()
	lastName = db.StringProperty()
	phoneNumber = db.StringProperty()
	password = db.StringProperty()
	joinDate = db.IntegerProperty()
	regID = db.StringProperty()
	
	def toDict(self):
		user = {}
		if self.is_saved():
			user["ID"] = self.key().id()
		user["userName"] = self.userName
		user["gender"] = self.gender
		user["email"] = self.email
		user["firstName"] = self.firstName
		user["lastName"] = self.lastName
		user["phoneNumber"] = self.phoneNumber
		user["password"] = self.password
		user["joinDate"] = self.joinDate
		user["regID"] = self.regID
		return user
		
class Message(db.Model):
	#REMEMBER TO CONVERT STRINGS TO intS/INTS WHEN YOU ARE WRITING THE REQUEST CLASSES!
	toID = db.IntegerProperty()
	fromID = db.IntegerProperty()
	text = db.StringProperty()
	isRead = db.IntegerProperty()
	messageID = db.IntegerProperty()
	
	def toDict(self):
		message = {}
		if self.is_saved():
			message["ID"] = self.key().id()
		message["toID"] = self.toID
		message["fromID"] = self.fromID
		message["text"] = self.text.encode('utf8')
		message["isRead"] = self.isRead
		message["messageID"] = self.messageID
		return message

class Trip(db.Model):
	#REMEMBER TO CONVERT STRINGS TO intS/INTS WHEN YOU ARE WRITING THE REQUEST CLASSES!
	driverID = db.IntegerProperty()
	maxPassengers = db.IntegerProperty()
	departureTime = db.IntegerProperty()
	departureTimeOfDay = db.IntegerProperty()
	departureDate = db.IntegerProperty()
	departureLocation = db.StringProperty()
	arrivalLocation = db.StringProperty()
	cost = db.IntegerProperty()
	roundTrip = db.IntegerProperty()
	returnTime = db.IntegerProperty()
	returnTimeOfDay = db.IntegerProperty()
	returnDate = db.IntegerProperty()
	oneTime = db.IntegerProperty()
	weekDays = db.StringProperty()
	
	def toDict(self):
		trip = {}
		if self.is_saved():
			trip["ID"] = self.key().id()
		trip["driverID"] = self.driverID
		trip["maxPassengers"] = self.maxPassengers
		trip["departureTime"] = self.departureTime
		trip["departureTimeOfDay"] = self.departureTimeOfDay
		trip["departureDate"] = self.departureDate
		trip["departureLocation"] = self.departureLocation
		trip["arrivalLocation"] = self.arrivalLocation
		trip["cost"] = self.cost
		trip["roundTrip"] = self.roundTrip
		trip["returnTime"] = self.returnTime
		trip["returnTimeOfDay"] = self.returnTimeOfDay
		trip["returnDate"] = self.returnDate
		trip["oneTime"] = self.oneTime
		trip["weekDays"] = self.weekDays
		
		return trip


class Passenger(db.Model):
	#REMEMBER TO CONVERT STRINGS TO intS/INTS WHEN YOU ARE WRITING THE REQUEST CLASSES!
	tripID = db.IntegerProperty()
	passengerID = db.IntegerProperty()
	rideID = db.IntegerProperty()
	isCheckedIn = db.IntegerProperty()
	def toDict(self):
		passenger = {}
		if self.is_saved():
			passenger["ID"] = self.key().id()
		passenger["tripID"] = self.tripID
		passenger["passengerID"] = self.passengerID
		passenger["rideID"] = self.rideID
		passenger["isCheckedIn"] = self.isCheckedIn
		return passenger

class Rating(db.Model):
	#REMEMBER TO CONVERT STRINGS TO intS/INTS WHEN YOU ARE WRITING THE REQUEST CLASSES!
	raterID = db.IntegerProperty()
	driverID = db.IntegerProperty()
	rating = db.IntegerProperty()
	def toDict(self):
		rating = {}
		if self.is_saved():
			rating["ID"] = self.key().id()
		rating["raterID"] = self.raterID
		rating["driverID"] = self.driverID
		rating["rating"] = self.rating
		return rating
	
class Ride(db.Model):
	#REMEMBER TO CONVERT STRINGS TO intS/INTS WHEN YOU ARE WRITING THE REQUEST CLASSES!
	riderID = db.IntegerProperty()
	departureTime = db.IntegerProperty()
	departureTimeOfDay = db.IntegerProperty()
	departureDate = db.IntegerProperty()
	departureLocation = db.StringProperty()
	arrivalLocation = db.StringProperty()
	cost = db.IntegerProperty()
	roundTrip = db.IntegerProperty()
	returnTime = db.IntegerProperty()
	returnTimeOfDay = db.IntegerProperty()
	returnDate = db.IntegerProperty()
	oneTime = db.IntegerProperty()
	weekDays = db.StringProperty()
	
	def toDict(self):
		ride = {}
		if self.is_saved():
			ride["ID"] = self.key().id()
		ride["riderID"] = self.riderID
		ride["departureTime"] = self.departureTime
		ride["departureTimeOfDay"] = self.departureTimeOfDay
		ride["departureDate"] = self.departureDate
		ride["departureLocation"] = self.departureLocation
		ride["arrivalLocation"] = self.arrivalLocation
		ride["cost"] = self.cost
		ride["roundTrip"] = self.roundTrip
		ride["returnTime"] = self.returnTime
		ride["returnTimeOfDay"] = self.returnTimeOfDay
		ride["returnDate"] = self.returnDate
		ride["oneTime"] = self.oneTime
		ride["weekDays"] = self.weekDays
		return ride
		
class HomePage(webapp2.RequestHandler):
	def get(self):
		self.response.write('<h1>SIGN UP!</h1>')
		self.response.write('Now that you are logged in, take the time to set up your profile!</br>')
		self.response.write('<h1>SET UP YOUR PROFILE</h1>')
		self.response.write('<form method = "post">')
		self.response.write('username: <input type = "text" name = "username"></br>')
		self.response.write('first name: <input type = "text" name = "firstname"></br>')
		self.response.write('last name: <input type = "text" name = "lastname"></br>')
		self.response.write('gender:</br> <input type="radio" name="sex" value="1">Male</br>')
		self.response.write('<input type="radio" name="sex" value="2">Female</br>')
		self.response.write('<input type="radio" name="sex" value="0">Unspecified</br>')
		self.response.write('phone number: <input type = "text" name = "phone"></br>')
		self.response.write('<input type="submit" value="Submit">')
		self.response.write('</form>')
		self.response.write('<a href ="')
		self.response.write('">Logout</a>')
		
	def post(self):
		self.response.write('<h1>Profile Set Up!</h1>')

#A JSON object representing the user to be searched is passed in as a parameter.
class UserHandler(webapp2.RequestHandler):
	def get(self,id):
		q = db.Query(User)
		if self.request.get('ID') != "0" and self.request.get('ID') != "":
			curUser = User.get_by_id(int(self.request.get('ID')))
			return self.response.out.write('['+json.dumps(curUser.toDict())+']')
		if self.request.get('userName') != "":
			q.filter('userName =',self.request.get('userName'))
		if self.request.get('gender') != "0" and self.request.get('gender') != "":
			q.filter('gender =',int(self.request.get('gender')))
		if self.request.get('email') != "":
			q.filter('email =',self.request.get('email'))
		if self.request.get('firstName') != "":
			q.filter('firstName =',self.request.get('firstName'))
		if self.request.get('lastName') != "":
			q.filter('lastName =',self.request.get('lastName'))
		if self.request.get('phoneNumber') != "":
			q.filter('phoneNumber =',self.request.get('phoneNumber'))
		if self.request.get('password') != "":
			q.filter('password =',self.request.get('password'))
		if self.request.get('joinDate') != "0" and self.request.get('joinDate') != "":
			q.filter('joinDate =',int(self.request.get('joinDate')))
		if self.request.get('regID') != "":
			q.filter('regID =',self.request.get('regID'))

		users_to_send = []
		for user in q:
			user.ID = user.key().id()
			users_to_send.append(user.toDict())
		return self.response.out.write(json.dumps(users_to_send))

	def post(self,id):
		user = User.all().filter('userName',self.request.get('userName')).filter('email',self.request.get('email')).get()
		if user == None:
			user = User(userName=self.request.get('userName'))
			if self.request.get('gender') != "":
				user.gender = int(self.request.get('gender'))
			else:
				user.gender = 0
			user.email = self.request.get('email')
			user.firstName = self.request.get('firstName')
			user.lastName = self.request.get('lastName')
			user.phoneNumber = self.request.get('phoneNumber')
			user.password = self.request.get('password')
			if self.request.get('joinDate') != "":
				user.joinDate = int(self.request.get('joinDate'))
			else:
				user.joinDate = 0
			user.regID = self.request.get('regID')
			user.put()
		return self.response.out.write(json.dumps(user.toDict()))

class TripHandler(webapp2.RequestHandler):
	def get(self,id):
		q = db.Query(Trip)
		if self.request.get('ID') != "0" and self.request.get('ID') != "":
			curTrip = Trip.get_by_id(int(self.request.get('ID')))
			return self.response.out.write('['+json.dumps(curTrip.toDict())+']')
		if self.request.get('driverID') != "" and self.request.get('driverID') != "0":
			q.filter('driverID =',int(self.request.get('driverID')))
			
		if self.request.get('maxPassengers') != "" and self.request.get('maxPassengers') != "0":
			q.filter('maxPassengers =',int(self.request.get('maxPassengers')))
			
		if self.request.get('departureTimeOfDay') != "" and self.request.get('departureTimeOfDay') != "0":
			q.filter('departureTimeOfDay =',int(self.request.get('departureTimeOfDay')))
		if self.request.get('departureTime') != "" and self.request.get('departureTime') != "0":
			q.filter('departureTime =',int(self.request.get('departureTime')))
			
		if self.request.get('departureDate') != "" and self.request.get('departureDate') != "0":
			q.filter('departureDate =',int(self.request.get('departureDate')))
		if self.request.get('departureLocation') != "":
			q.filter('departureLocation =',self.request.get('departureLocation'))
		if self.request.get('arrivalLocation') != "":
			q.filter('arrivalLocation =',self.request.get('arrivalLocation'))
		if self.request.get('cost') != "" and self.request.get('cost') != "0":
			q.filter('cost =',int(self.request.get('cost'))) #note the less than or equal to operator
		if self.request.get('roundTrip') != "0" and self.request.get('roundTrip') != "":
			q.filter('roundTrip =',int(self.request.get('roundTrip')))
			
		if self.request.get('returnTimeOfDay') != "" and self.request.get('returnTimeOfDay') != "0":
			q.filter('returnTimeOfDay =',int(self.request.get('returnTimeOfDay')))
		if self.request.get('returnTime') != "" and self.request.get('returnTime') != "0":
			q.filter('returnTime =',int(self.request.get('returnTime')))
			
		if self.request.get('returnDate') != "" and self.request.get('returnDate') != "0":
			q.filter('returnDate =',int(self.request.get('returnDate')))
		if self.request.get('oneTime') != "0" and self.request.get('oneTime') != "":
			q.filter('oneTime =',int(self.request.get('oneTime')))
		if self.request.get('weekDays') != "":
			q.filter('weekDays =',self.request.get('weekDays'))
		trips_to_send = []
		for trip in q:
			trip.ID = trip.key().id()
			trips_to_send.append(trip.toDict())
		return self.response.out.write(json.dumps(trips_to_send))

	def post(self,id):
		trip = Trip()
		if self.request.get('driverID') != "":
			trip.driverID=int(self.request.get('driverID'))
		if self.request.get('maxPassengers') != "":
			trip.maxPassengers = int(self.request.get('maxPassengers'))
		if self.request.get('departureTime') != "":
			trip.departureTime = int(self.request.get('departureTime'))
			if trip.departureTime >= 0 and trip.departureTime <= 359:
				trip.departureTimeOfDay = 1
			elif trip.departureTime >= 400 and trip.departureTime <= 759:
				trip.departureTimeOfDay = 2
			elif trip.departureTime >= 800 and trip.departureTime <= 1159:
				trip.departureTimeOfDay = 3
			elif trip.departureTime >= 1200 and trip.departureTime <= 1559:
				trip.departureTimeOfDay = 4
			elif trip.departureTime >= 1600 and trip.departureTime <= 1959:
				trip.departureTimeOfDay = 5
			elif trip.departureTime >= 2000 and trip.departureTime <= 2359:
				trip.departureTimeOfDay = 6
		if self.request.get('departureDate') != "":
			trip.departureDate = int(self.request.get('departureDate'))
		if self.request.get('departureLocation') != "":
			trip.departureLocation = self.request.get('departureLocation')
		if self.request.get('arrivalLocation') != "":
			trip.arrivalLocation = self.request.get('arrivalLocation')
		if self.request.get('cost') != "":
			trip.cost = int(self.request.get('cost'))
		if self.request.get('roundTrip') != "":
			trip.roundTrip = int(self.request.get('roundTrip'))
		else:
			trip.roundTrip = 0
		if self.request.get('returnTime') != "":
			trip.returnTime = int(self.request.get('returnTime'))
			if trip.returnTime >= 0 and trip.returnTime <= 359:
				trip.returnTimeOfDay = 1
			elif trip.returnTime >= 400 and trip.returnTime <= 759:
				trip.returnTimeOfDay = 2
			elif trip.returnTime >= 800 and trip.returnTime <= 1159:
				trip.returnTimeOfDay = 3
			elif trip.returnTime >= 1200 and trip.returnTime <= 1559:
				trip.returnTimeOfDay = 4
			elif trip.returnTime >= 1600 and trip.returnTime <= 1959:
				trip.returnTimeOfDay = 5
			elif trip.returnTime >= 2000 and trip.returnTime <= 2359:
				trip.returnTimeOfDay = 6
		if self.request.get('returnDate') != "":
			trip.returnDate = int(self.request.get('returnDate'))
		if self.request.get('oneTime') != "":
			trip.oneTime = int(self.request.get('oneTime'))
		else:
			trip.oneTime = 0;
		if self.request.get('weekDays') != "":
			trip.weekDays = self.request.get('weekDays')
		else:
			trip.weekDays = ""
		trip.put()
		return self.response.out.write(json.dumps(trip.toDict()))
		
	def delete(self,id):
		trip = Trip()
		if(self.request.get('ID') != ""):
			trip = Trip.get_by_id(int(self.request.get('ID')))
			trip.delete()
		return self.response.out.write(json.dumps(trip.toDict()))
		
class RideHandler(webapp2.RequestHandler):
	def get(self,id):
		q = db.Query(Ride)
		if self.request.get('ID') != "0" and self.request.get('ID') != "":
			curRide = Ride.get_by_id(int(self.request.get('ID')))
			return self.response.out.write('['+json.dumps(curRide.toDict())+']')
		if self.request.get('driverID') != "" and self.request.get('driverID') != "0":
			q.filter('driverID =',int(self.request.get('driverID')))
		if self.request.get('departureTime') != "" and self.request.get('departureTime') != "0":
			q.filter('departureTime =',int(self.request.get('departureTime')))
		if self.request.get('departureDate') != "" and self.request.get('departureDate') != "0":
			q.filter('departureDate =',int(self.request.get('departureDate')))
		if self.request.get('departureLocation') != "":
			q.filter('departureLocation =',self.request.get('departureLocation'))
		if self.request.get('arrivalLocation') != "":
			q.filter('arrivalLocation =',self.request.get('arrivalLocation'))
		if self.request.get('cost') != "" and self.request.get('cost') != "0":
			q.filter('cost =',int(self.request.get('cost'))) #note the greater than or equal to operator
		if self.request.get('roundTrip') != "0" and self.request.get('roundTrip') != "":
			q.filter('roundTrip =',int(self.request.get('roundTrip')))
		if self.request.get('returnTime') != "" and self.request.get('returnTime') != "0":
			q.filter('returnTime =',int(self.request.get('returnTime')))
		if self.request.get('returnDate') != "" and self.request.get('returnDate') != "0":
			q.filter('returnDate =',int(self.request.get('returnDate')))
		if self.request.get('oneTime') != "0" and self.request.get('oneTime') != "":
			q.filter('oneTime =',int(self.request.get('oneTime')))
		if self.request.get('weekDays') != "":
			q.filter('weekDays =',self.request.get('weekDays'))

		rides_to_send = []
		for ride in q:
			rides_to_send.append(ride.toDict())
		return self.response.out.write(json.dumps(rides_to_send))

	def post(self,id):
		ride = Ride()
		if self.request.get('riderID') != "":
			ride.riderID=int(self.request.get('riderID'))
		if self.request.get('departureTime') != "":
			ride.departureTime = int(self.request.get('departureTime'))
		if self.request.get('departureTimeOfDay') != "":
			ride.departureTimeOfDay = int(self.request.get('departureTimeOfDay'))
		else:
			ride.departureTimeOfDay = 0
		if self.request.get('departureDate') != "":
			ride.departureDate = int(self.request.get('departureDate'))
		if self.request.get('departureLocation') != "":
			ride.departureLocation = self.request.get('departureLocation')
		if self.request.get('arrivalLocation') != "":
			ride.arrivalLocation = self.request.get('arrivalLocation')
		if self.request.get('cost') != "":
			ride.cost = int(self.request.get('cost'))
		if self.request.get('roundTrip') != "":
			ride.roundTrip = int(self.request.get('roundTrip'))
		else:
			ride.roundTrip = 0;
		if self.request.get('returnTime') != "":
			ride.returnTime = int(self.request.get('returnTime'))
		if self.request.get('returnTimeOfDay') != "":
			ride.returnTimeOfDay = int(self.request.get('returnTimeOfDay'))
		else:
			ride.returnTimeOfDay = 0
		if self.request.get('returnDate') != "":
			ride.returnDate = int(self.request.get('returnDate'))
		if self.request.get('oneTime') != "":
			ride.oneTime = int(self.request.get('oneTime'))
		else:
			ride.oneTime = 0
		if self.request.get('weekDays') != "":
			ride.weekDays = self.request.get('weekDays')
		else:
			ride.weekDays = ""
		ride.put()
		return self.response.out.write(json.dumps(ride.toDict()))
		
	def delete(self,id):
		ride = Ride()
		if(self.request.get('ID') != ""):
			ride = Ride.get_by_id(int(self.request.get('ID')))
			ride.delete()
		return self.response.out.write(json.dumps(ride.toDict()))
		
class PassengerHandler(webapp2.RequestHandler):
	def get(self,id):
		q = db.Query(Passenger)
		if self.request.get('ID') != "0" and self.request.get('ID') != "":
			curPassenger = Passenger.get_by_id(int(self.request.get('ID')))
			return self.response.out.write('['+json.dumps(curPassenger.toDict())+']')
		if self.request.get('tripID') != "0" and self.request.get('tripID') != "":
			q.filter('tripID =',int(self.request.get('tripID')))
		if self.request.get('passengerID') != "0" and self.request.get('passengerID') != "":
			q.filter('passengerID =',int(self.request.get('passengerID')))
		if self.request.get('rideID') != "0" and self.request.get('rideID') != "":
			q.filter('rideID =',int(self.request.get('rideID')))
		if self.request.get('isCheckedIn') != "0" and self.request.get('isCheckedIn') != "":
			q.filter('isCheckedIn =',int(self.request.get('isCheckedIn')))

		passengers_to_send = []
		for passenger in q:
			passenger.ID = passenger.key().id()
			passengers_to_send.append(passenger.toDict())
		return self.response.out.write(json.dumps(passengers_to_send))

	def post(self,id):
		passenger = Passenger(tripID=int(self.request.get('tripID')))
		passenger.passengerID = int(self.request.get('passengerID'))
		if self.request.get('rideID') != "" and self.request.get('rideID') != "0":
			passenger.rideID = int(self.request.get('rideID'))
		else:
			passenger.rideID = 0;
		passenger.isCheckedIn = int(self.request.get('isCheckedIn'))
		passenger.put()
		return self.response.out.write(json.dumps(passenger.toDict()))
	def put(self,id):
		passenger = Passenger.get_by_id(int(self.request.get('ID')))
		passenger.tripID =int(self.request.get('tripID'))
		passenger.passengerID = int(self.request.get('passengerID'))
		if self.request.get('rideID') != "" and self.request.get('rideID') != "0":
			passenger.rideID = int(self.request.get('rideID'))
		else:
			passenger.rideID = 0;
		passenger.isCheckedIn = int(self.request.get('isCheckedIn'))
		passenger.put()
		return self.response.out.write(json.dumps(passenger.toDict()))
	def delete(self,id):
		passenger = Passenger()
		if(self.request.get('ID') != ""):
			passenger = Passenger.get_by_id(int(self.request.get('ID')))
			passenger.delete()
		return self.response.out.write(json.dumps(passenger.toDict()))
		
class RatingHandler(webapp2.RequestHandler):
	def get(self,id):
		q = db.Query(Rating)
		if self.request.get('ID') != "0" and self.request.get('ID') != "":
			curRating = Rating.get_by_id(int(self.request.get('ID')))
			return self.response.out.write('['+json.dumps(curRating.toDict())+']')
		if self.request.get('driverID') != "0" and self.request.get('driverID') != "":
			q.filter('driverID =',int(self.request.get('driverID')))
		if self.request.get('raterID') != "0" and self.request.get('raterID') != "":
			q.filter('raterID =',int(self.request.get('raterID')))
		if self.request.get('rating') != "0" and self.request.get('rating') != "":
			q.filter('rating =',int(self.request.get('rating')))

		ratings_to_send = []
		for rating in q:
			rating.ID = rating.key().id()
			ratings_to_send.append(rating.toDict())
		return self.response.out.write(json.dumps(ratings_to_send))

	def post(self,id):
		rating = Rating(driverID=int(self.request.get('driverID')))
		rating.raterID = int(self.request.get('raterID'))
		rating.rating = int(self.request.get('rating'))
		rating.put()
		return self.response.out.write(json.dumps(rating.toDict()))
		
class MessageHandler(webapp2.RequestHandler):
	def get(self,id):
		q = db.Query(Message)
		if self.request.get('ID') != "0" and self.request.get('ID') != "":
			curMessage = Message.get_by_id(int(self.request.get('ID')))
			return self.response.out.write('['+json.dumps(curMessage.toDict())+']')
		if self.request.get('toID') != "0" and self.request.get('toID') != "":
			q.filter('toID =',int(self.request.get('toID')))
		if self.request.get('fromID') != "0" and self.request.get('fromID') != "":
			q.filter('fromID =',int(self.request.get('fromID')))
		if self.request.get('text') != "" and self.request.get('text') != "null":
			q.filter('text =',self.request.get('text'))
		if self.request.get('isRead') != "0" and self.request.get('isRead') != "":
			q.filter('isRead =',int(self.request.get('isRead')))
		if self.request.get('messageID') != "0" and self.request.get('messageID') != "":
			q.filter('messageID =',int(self.request.get('messageID')))

		messages_to_send = []
		for message in q:
			message.ID = message.key().id()
			messages_to_send.append(message.toDict())
		return self.response.out.write(json.dumps(messages_to_send))

	def post(self,id):
		message = Message(toID=int(self.request.get('toID')))
		message.fromID = int(self.request.get('fromID'))
		message.text = self.request.get('text').decode('utf8')
		if self.request.get('isRead') != "":
			message.isRead = int(self.request.get('isRead'))
		else:
			message.isRead = 0
		if self.request.get('messageID') != "":
			message.messageID = int(self.request.get('messageID'))
		else:
			message.messageID = 0
		message.put()
		return self.response.out.write(json.dumps(message.toDict()))
		
	def delete(self,id):
		message = Message()
		if(self.request.get('ID') != ""):
			message = Message.get_by_id(int(self.request.get('ID')))
			message.delete()
		return self.response.out.write(json.dumps(message.toDict()))
		
	def put(self,id):
		message = Message.get_by_id(int(self.request.get('ID')))
		message.toID = int(self.request.get('toID'))
		message.fromID = int(self.request.get('fromID'))
		message.text = self.request.get('text').decode('utf8')
		if self.request.get('isRead') != "":
			message.isRead = int(self.request.get('isRead'))
		else:
			message.isRead = 0
		if self.request.get('messageID') != "":
			message.messageID = int(self.request.get('messageID'))
		else:
			message.messageID = 0
		message.put()
		return self.response.out.write(json.dumps(message.toDict()))	

application = webapp2.WSGIApplication([
	('/', HomePage),
	('/users\/?([0-9]*)', UserHandler),
	('/trips\/?([0-9]*)', TripHandler),
	('/rides\/?([0-9]*)', RideHandler),
	('/passengers\/?([0-9]*)', PassengerHandler),
	('/ratings\/?([0-9]*)', RatingHandler),
	('/messages\/?([0-9]*)', MessageHandler)
], debug=True)
