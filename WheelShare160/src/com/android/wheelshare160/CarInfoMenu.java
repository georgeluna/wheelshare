package com.android.wheelshare160;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class CarInfoMenu extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_car_info_menu);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.car_info_menu, menu);
		return true;
	}
	
    public void OnSubmitTripInfo(View view) {
        // Do something in response to button
    	
    	// FOR NOW JUST REROUTING BACK TO LOGIN PAGE
		
    	Intent intent = new Intent(this, Login.class);
    	//EditText editText = (EditText) findViewById(R.id.textfield_username);
    	//String username = editText.getText().toString();
    	//intent.putExtra(EXTRA_USERNAME, username);
    	//new EndpointsTask().execute(getApplicationContext());
        startActivity(intent);
    }

}
