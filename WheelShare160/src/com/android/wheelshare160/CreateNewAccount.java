package com.android.wheelshare160;

import com.wheelshare.objects.*;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;

public class CreateNewAccount extends Activity {
	
    public final static String EXTRA_USERNAME = "com.android.wheelshare160.USERNAME";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		ObjectManager.setCurrentActivity(this);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_new_account);
		// Show the Up button in the action bar.
		setupActionBar();
	}
	
	public void SubmitData(View view)
	{
		// Do something with data submitted
		// Then go to main menu
		Intent intent = new Intent(this, SeekingOfferingRides.class);
		EditText userName = (EditText) findViewById(R.id.usernameText);
		EditText password = (EditText) findViewById(R.id.passwordText1);
		EditText retypePassword = (EditText) findViewById(R.id.passwordText2);
		EditText firstName = (EditText) findViewById(R.id.firstnameText);
		EditText lastName = (EditText) findViewById(R.id.lastnameText);
		EditText email = (EditText) findViewById(R.id.emailText);
		EditText phoneNumber = (EditText) findViewById(R.id.phonenumberText);
		int gender = 0;
		RadioButton currentRadio = (RadioButton) findViewById(R.id.maleRadioButton);
		boolean maleChecked = currentRadio.isChecked();
		currentRadio = (RadioButton) findViewById(R.id.femaleRadioButton);
		boolean femaleChecked = currentRadio.isChecked();
		if(maleChecked) gender = 1;
		else if(femaleChecked) gender = 2;
		User newUser = new User();
		newUser.setFirstName(firstName.getText().toString());
		newUser.setLastName(lastName.getText().toString());
		newUser.setPassword(password.getText().toString());
		newUser.setUserName(userName.getText().toString());
		newUser.setEmail(email.getText().toString());
		newUser.setPhoneNumber(phoneNumber.getText().toString());
		newUser.setGender(gender);
		newUser.setJoinDate((long) (System.currentTimeMillis() / 1000L));
		int registered = ObjectManager.registerUser(newUser,retypePassword.getText().toString());
		if(registered == 1)
		{
			intent.putExtra(EXTRA_USERNAME, ObjectManager.currentUser.getFirstName());
			startActivity(intent);
		}
		else
		{
		    if(registered == 0) displayError(getResources().getString(R.string.cna_passworderror0));
		    else if(registered == -1) displayError(getResources().getString(R.string.cna_passworderror1));
		    else if(registered == -2) displayError(getResources().getString(R.string.cna_passworderror2));
		    else if(registered == -3) displayError(getResources().getString(R.string.cna_passworderror3init) + " " +
		    		newUser.getUserName() + " " + getResources().getString(R.string.cna_passworderror3));
		    else if(registered == -4) displayError(getResources().getString(R.string.cna_passworderror4init) + " " +
		    		newUser.getEmail() + " " + getResources().getString(R.string.cna_passworderror4));
		    else if(registered == -5) displayError(getResources().getString(R.string.cna_passworderror5));
		}
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.create_new_account, menu);
		return true;
	}
	
	public void displayError(String message)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setTitle(getResources().getString(R.string.title_error));
	    builder.setMessage(message);
	    
	    builder.setNeutralButton(getResources().getString(R.string.cna_ok), new DialogInterface.OnClickListener()
	    {
	        @Override
	        public void onClick(DialogInterface dialog, int whichButton)
	        {
	        	return;
	        }
	    });
	    builder.create().show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	 public void onBackPressed()
	 {
		 Intent intent = new Intent(this, Login.class);
		 startActivity(intent);
	 }

}
