package com.android.wheelshare160;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import com.android.wheelshare160.R;
import com.android.wheelshare160.util.SystemUiHider;
import com.wheelshare.objects.*;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class Login extends Activity {
	
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = false;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * If set, will toggle the system UI visibility upon interaction. Otherwise,
     * will show the system UI visibility upon interaction.
     */
    private static final boolean TOGGLE_ON_CLICK = true;

    /**
     * The flags to pass to {@link SystemUiHider#getInstance}.
     */
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

    /**
     * The instance of the {@link SystemUiHider} for this activity.
     */
    private SystemUiHider mSystemUiHider;
    
    /**
     * In order for the next activity to query the extra data, you should
     * define the key for your intent's extra using a public constant
     */
    public final static String EXTRA_USERNAME = "com.android.wheelshare160.USERNAME";
    


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ObjectManager.setCurrentActivity(this);
        setContentView(R.layout.activity_login);

        final View controlsView = findViewById(R.id.fullscreen_content_controls);
        final View contentView = findViewById(R.id.fullscreen_content);

        // Set up an instance of SystemUiHider to control the system UI for
        // this activity.
        mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
        mSystemUiHider.setup();
        mSystemUiHider
                .setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
                    // Cached values.
                    int mControlsHeight;
                    int mShortAnimTime;

                    @Override
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
                    public void onVisibilityChange(boolean visible) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                            // If the ViewPropertyAnimator API is available
                            // (Honeycomb MR2 and later), use it to animate the
                            // in-layout UI controls at the bottom of the
                            // screen.
                            if (mControlsHeight == 0) {
                                mControlsHeight = controlsView.getHeight();
                            }
                            if (mShortAnimTime == 0) {
                                mShortAnimTime = getResources().getInteger(
                                        android.R.integer.config_shortAnimTime);
                            }
                            controlsView.animate()
                                    .translationY(visible ? 0 : mControlsHeight)
                                    .setDuration(mShortAnimTime);
                        } else {
                            // If the ViewPropertyAnimator APIs aren't
                            // available, simply show or hide the in-layout UI
                            // controls.
                            controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
                        }

//                        if (visible && AUTO_HIDE) {
//                            // Schedule a hide().
//                            delayedHide(AUTO_HIDE_DELAY_MILLIS);
//                        }
                    }
                });
  
        
        // Set up the user interaction to manually show or hide the system UI.
        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TOGGLE_ON_CLICK) {
                    mSystemUiHider.toggle();
                } else {
                    mSystemUiHider.show();
                }
            }
        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        findViewById(R.id.login_button).setOnTouchListener(mDelayHideTouchListener);
        
        
    }

    /** Called when the user clicks the Login to WheelShare button */
    
    public void LoggingIn(View view)
    {
    	EditText editText = (EditText) findViewById(R.id.textfield_username);
    	EditText editText2 = (EditText) findViewById(R.id.textfield_password);
    	String username = editText.getText().toString();
    	String password = editText2.getText().toString();
    	
    	/*Trip newTrip = new Trip();
    	newTrip.setID(22002);
    	
    	List<Trip> tripResults = ObjectManager.search(newTrip);
    	ViewTrip.currentTrip = tripResults.get(0);
    	Passenger queryPassenger = new Passenger();
		queryPassenger.setTripID(22002);
		ViewTrip.currentPassengerList = ObjectManager.search(queryPassenger);*/
    	
    	
    	Log.i("wheelshare160","Attempting to log in as " + username);
    	int userVerificationResults = ObjectManager.verifyUser(username,password);
    	if(userVerificationResults == 1) showDialog(getResources().getString(R.string.cna_successfullogin) + ObjectManager.currentUser.getUserName(),false);
    	else
    	{
    		
    		
    		
    		
    		if(userVerificationResults == 0) showDialog(getResources().getString(R.string.cna_incorrectpassword),true);
    		else if(userVerificationResults == -1) showDialog(getResources().getString(R.string.cna_usernotexist),true);
    		else if(userVerificationResults == -2) showDialog(getResources().getString(R.string.cna_blankusernamepassword),true);
    	}
	}
    
/** Called when the user clicks the Login to WheelShare button */
    
    /** Called when the user clicks the Create New Account button */
    
    public void onRegister(View view) {
            // Do something in response to button
    		
        	Intent intent = new Intent(this, CreateNewAccount.class);
        	//EditText editText = (EditText) findViewById(R.id.textfield_username);
        	//String username = editText.getText().toString();
        	//intent.putExtra(EXTRA_USERNAME, username);
        	//new EndpointsTask().execute(getApplicationContext());
            startActivity(intent);
        }
    
    
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }


    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    Handler mHideHandler = new Handler();
    Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            mSystemUiHider.hide();
        }
    };

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
    
    void showDialog(final String message, boolean error)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
		if(error)
		{
			builder.setTitle(getResources().getString(R.string.title_error));
			builder.setMessage(message)
		       .setCancelable(false)
		       .setNeutralButton(getResources().getString(R.string.cna_ok), new DialogInterface.OnClickListener()
		       {
		           public void onClick(DialogInterface dialog, int id)
		           {
		        	   Log.e("wheelshare160", "ERROR: " + message);
		           }
		       });
		}
		else
		{
			builder.setTitle(getResources().getString(R.string.title_success));
			builder.setMessage(message)
		       .setCancelable(false)
		       .setNeutralButton(getResources().getString(R.string.cna_ok), new DialogInterface.OnClickListener()
		       {
		           public void onClick(DialogInterface dialog, int id)
		           {
		        	   Log.i("wheelshare160", "SUCCESS: " + message);
		        	   Intent intent = new Intent(Login.this, SeekingOfferingRides.class);
		        	   startActivity(intent);
		           }
		       });
		}
		
		AlertDialog alert = builder.create();
		alert.show();
	}
}
