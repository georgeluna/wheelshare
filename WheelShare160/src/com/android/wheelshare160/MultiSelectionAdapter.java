package com.android.wheelshare160;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

public class MultiSelectionAdapter<Ride> extends BaseAdapter
{
	Context mContext;
	LayoutInflater mInflater;
	List<Ride> mList;
	SparseBooleanArray mSparseBooleanArray;
	
	public MultiSelectionAdapter(Context context, List<Ride> list)
	{
		// TODO Auto-generated constructor stub
		this.mContext = context;
		mInflater = LayoutInflater.from(mContext);
		mSparseBooleanArray = new SparseBooleanArray();
		this.mList = list;
	}
	
	public ArrayList<Ride> getCheckedItems()
	{
		ArrayList<Ride> mTempArry = new ArrayList<Ride>();
		for(int i=0;i<mList.size();i++)
		{
			if(mSparseBooleanArray.get(i))
			{
				mTempArry.add(mList.get(i));
			}
		}
		return mTempArry;
	}
	
	@Override
	public int getCount()
	{
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Ride getItem(int position)
	{
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		// TODO Auto-generated method stub
		if(convertView == null)
		{
			convertView = mInflater.inflate(R.layout.ride_item, null);
		}
		TextView tvTitle = (TextView) convertView.findViewById(R.id.checkableRideTextView);
		tvTitle.setText(mList.get(position).toString());
		CheckBox mCheckBox = (CheckBox) convertView.findViewById(R.id.checkableRideCheckBox);
		mCheckBox.setTag(position);
		mCheckBox.setChecked(mSparseBooleanArray.get(position));
		mCheckBox.setOnCheckedChangeListener(mCheckedChangeListener);
		return convertView;
	}
	
	OnCheckedChangeListener mCheckedChangeListener = new OnCheckedChangeListener()
	{
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
		{
			// TODO Auto-generated method stub
			mSparseBooleanArray.put((Integer) buttonView.getTag(), isChecked);
		}
	};
}

