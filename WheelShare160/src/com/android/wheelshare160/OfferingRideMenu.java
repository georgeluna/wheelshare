package com.android.wheelshare160;

import android.app.TimePickerDialog;
import android.app.DatePickerDialog;
import java.util.Calendar;
import android.os.Bundle;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import com.android.wheelshare160.R;
import com.wheelshare.objects.ObjectManager;
import com.wheelshare.objects.Trip;

public class OfferingRideMenu extends FragmentActivity {
	
	public static class TimePickerFragment extends DialogFragment
    implements TimePickerDialog.OnTimeSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current time as the default values for the picker
			final Calendar c = Calendar.getInstance();
			int hour = c.get(Calendar.HOUR_OF_DAY);
			int minute = c.get(Calendar.MINUTE);

			// Create a new instance of TimePickerDialog and return it
			return new TimePickerDialog(getActivity(), this, hour, minute,DateFormat.is24HourFormat(getActivity()));
		}

		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// Do something with the time chosen by the user
		}
	}
	
	public static class DatePickerFragment extends DialogFragment
    implements DatePickerDialog.OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			// Create a new instance of DatePickerDialog and return it
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		public void onDateSet(DatePicker view, int year, int month, int day) {
			// Do something with the date chosen by the user
		}
	}
	
	
	Trip currentTrip = new Trip();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_offering_ride_menu);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.offering_ride_menu, menu);
		return true;
	}
	

    /** Called when the user clicks the Create New Account button */
    
    public void SubmitData(View view) {
    	
    	currentTrip.setDriverID(ObjectManager.currentUser.getID());
    	
		EditText departureLocation = (EditText) findViewById(R.id.offeringDepartureLocationText);
		EditText arrivalLocation = (EditText) findViewById(R.id.offeringArrivalLocationText);
		
		EditText cost = (EditText) findViewById(R.id.offeringSetCostText);
		if(!cost.getText().toString().equals("")) currentTrip.setCost(Integer.parseInt(cost.getText().toString()));
		else currentTrip.setCost(0);
		
		EditText maxPassengers = (EditText) findViewById(R.id.offeringSetMaxPassengersText);
		if(!maxPassengers.getText().toString().equals("")) currentTrip.setMaxPassengers(Integer.parseInt(maxPassengers.getText().toString()));
		else currentTrip.setMaxPassengers(1);
		
		currentTrip.setDepartureLocation(departureLocation.getText().toString());
		currentTrip.setArrivalLocation(arrivalLocation.getText().toString());
		
		CheckBox currentCheck = (CheckBox) findViewById(R.id.offeringCheckRoundTrip);
		
		if(currentCheck.isChecked())
		{
			currentTrip.setRoundTrip(1);
		}
		else currentTrip.setRoundTrip(0);
		
		currentCheck = (CheckBox) findViewById(R.id.offeringCheckOneTime);
		
		if(currentCheck.isChecked())
		{
			String weekDays = "";
			CheckBox checkDay = (CheckBox) findViewById(R.id.offeringCheckSunday);
			if(checkDay.isChecked()) weekDays += "1";
			else weekDays += "0";
			checkDay = (CheckBox) findViewById(R.id.offeringCheckMonday);
			if(checkDay.isChecked()) weekDays += "1";
			else weekDays += "0";
			checkDay = (CheckBox) findViewById(R.id.offeringCheckTuesday);
			if(checkDay.isChecked()) weekDays += "1";
			else weekDays += "0";
			checkDay = (CheckBox) findViewById(R.id.offeringCheckWednesday);
			if(checkDay.isChecked()) weekDays += "1";
			else weekDays += "0";
			checkDay = (CheckBox) findViewById(R.id.offeringCheckThursday);
			if(checkDay.isChecked()) weekDays += "1";
			else weekDays += "0";
			checkDay = (CheckBox) findViewById(R.id.offeringCheckFriday);
			if(checkDay.isChecked()) weekDays += "1";
			else weekDays += "0";
			checkDay = (CheckBox) findViewById(R.id.offeringCheckSaturday);
			if(checkDay.isChecked()) weekDays += "1";
			else weekDays += "0";
			currentTrip.setOneTime(1);
			currentTrip.setWeekDays(weekDays);
		}
		else
		{
			currentTrip.setOneTime(0);
			currentTrip.setWeekDays("");
		}
		
		ViewOfferedTrip.currentTrip = currentTrip;
		Intent intent = new Intent(OfferingRideMenu.this, ViewOfferedTrip.class);
        startActivity(intent);
    }
    
    public void SetDepartureTime(View view)
	{
    	DialogFragment newFragment = new TimePickerFragment()
    	{
    		@Override
    		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
    			currentTrip.setDepartureTime((hourOfDay * 100) + minute);
    		}
    	};
        newFragment.show(getSupportFragmentManager(), "timePicker");
	}
    
    public void SetDepartureDate(View view)
	{
    	DialogFragment newFragment = new DatePickerFragment()
    	{
    		@Override
    		public void onDateSet(DatePicker view, int year, int month, int day) {
    			currentTrip.setDepartureDate((year * 10000) + (month * 100) + day);
    		}
    	};
        newFragment.show(getSupportFragmentManager(), "datePicker");
	}
    
    public void SetReturnTime(View view)
	{
    	CheckBox currentCheck = (CheckBox) findViewById(R.id.offeringCheckRoundTrip);
		if(currentCheck.isChecked())
		{
	    	DialogFragment newFragment = new TimePickerFragment()
	    	{
	    		@Override
	    		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	    			currentTrip.setReturnTime((hourOfDay * 100) + minute);
	    		}
	    	};
	        newFragment.show(getSupportFragmentManager(), "timePicker");
		}
	}
    
    public void SetReturnDate(View view)
	{
    	CheckBox currentCheck = (CheckBox) findViewById(R.id.offeringCheckRoundTrip);
		if(currentCheck.isChecked())
		{
	    	DialogFragment newFragment = new DatePickerFragment()
	    	{
	    		@Override
	    		public void onDateSet(DatePicker view, int year, int month, int day) {
	    			currentTrip.setReturnDate((year * 10000) + (month * 100) + day);
	    		}
	    	};
	        newFragment.show(getSupportFragmentManager(), "datePicker");
		}
	}
    
    @Override
	 public void onBackPressed()
	 {
		 Intent intent = new Intent(this, SeekingOfferingRides.class);
		 startActivity(intent);
	 }
}
