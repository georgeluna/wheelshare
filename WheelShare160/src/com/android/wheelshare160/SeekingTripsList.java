package com.android.wheelshare160;

//http://stackoverflow.com/questions/5070830/populating-a-listview-using-arraylist
//Also, read the following:
//http://stackoverflow.com/questions/456211/activity-restart-on-rotation-android - LOL WUT

import java.util.ArrayList;
import java.util.List;
import com.wheelshare.objects.*;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.widget.AdapterView; 
import android.widget.AdapterView.OnItemClickListener;


public class SeekingTripsList extends Activity {
	
	static Trip currentTrip;
	//another thing I learned: lists cannot be instantiated because they are interfaces!
	static List<Trip> currentTripList = new ArrayList<Trip>();
	List<User> currentUserList = new ArrayList<User>();
	public class TripListAdapter extends ArrayAdapter<Trip> {

	    private List<Trip> items;
	    private Context context;

	    public TripListAdapter(Context context, int textViewResourceId, List<Trip> items) {
	        super(context, textViewResourceId, items);
	        this.context = context;
	        this.items = items;
	    }

	    public View getView(int position, View convertView, ViewGroup parent) {
	        View view = convertView;
	        if (view == null) {
	            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            view = inflater.inflate(R.layout.trip_item, null);
	        }
	        //Log.i("wheelshare160","WHY IS THIS HAPPENING?" + String.valueOf(position) + " " + String.valueOf(items.size()) + " " + String.valueOf(currentUserList.size()));
	        Trip trip = items.get(position);
	        User user = currentUserList.get(position);
	        if (trip!= null) {
	            // My layout has only one TextView
	            TextView itemView = (TextView) view.findViewById(R.id.tripItemTitleView);
	            if (itemView != null) {
	                itemView.setText(trip.getArrivalLocation() + " on " + ObjectManager.getDate(trip.getDepartureDate()) +
	                				 " at " + ObjectManager.getTime(trip.getDepartureTime()));
	            }
	            
	            itemView = (TextView) view.findViewById(R.id.tripItemDepartureView);
	            if (itemView != null) {
	                itemView.setText("Offered by " + user.getFirstName() + " " + user.getLastName() + " from " + trip.getDepartureLocation());
	            }
	            
	            
	         }

	        return view;
	    }
	}

	private ListView tripListView;
	TripListAdapter tripListAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_trips_list);
		// Show the Up button in the action bar.
		setupActionBar();
    	
		tripListView =  (ListView) findViewById(R.id.tripListView);
		
    	//currentTripList = ObjectManager.search(currentTrip);
    	
    	currentUserList.clear();
    	User queryUser = new User();
    	for(int i = 0; i < currentTripList.size(); i++)
    	{
    		queryUser.setID(currentTripList.get(i).getDriverID());
    		currentUserList.add(ObjectManager.search(queryUser).get(0));
    	}
        
    	tripListAdapter = new TripListAdapter(this,android.R.layout.simple_list_item_1, currentTripList);
    	tripListView.setAdapter(tripListAdapter);
    	tripListView.setOnItemClickListener(new OnItemClickListener()
    	{
    		public void onItemClick(AdapterView<?> myAdapter, View myView, int position, long mylng)
    		{
    			Trip selectedTrip = (Trip)(tripListView.getItemAtPosition(position));
    			Log.i("wheelshare160",selectedTrip.toString());
    			
    			if(selectedTrip.getDriverID() == ObjectManager.currentUser.getID())
    			{
    				ViewTrip.searchResult = true;
    				ViewTrip.currentTrip = selectedTrip;
    				Intent intent = new Intent(SeekingTripsList.this, ViewTrip.class);
		        	startActivity(intent);
    			}
    			else confirmAddTrip(selectedTrip);	
    		}                 
    	});
    	
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.objects_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	void showDialog(final String message, boolean error)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(SeekingTripsList.this);
		if(error)
		{
			builder.setTitle(getResources().getString(R.string.title_error));
			builder.setMessage(message)
		       .setCancelable(false)
		       .setNeutralButton("Okay", new DialogInterface.OnClickListener()
		       {
		           public void onClick(DialogInterface dialog, int id)
		           {
		        	   Log.e("wheelshare160", "ERROR: " + message);
		           }
		       });
		}
		else
		{
			builder.setTitle(getResources().getString(R.string.title_success));
			builder.setMessage(message)
		       .setCancelable(false)
		       .setNeutralButton(getResources().getString(R.string.cna_ok), new DialogInterface.OnClickListener()
		       {
		           public void onClick(DialogInterface dialog, int id)
		           {
		        	   Log.i("wheelshare160", "SUCCESS: " + message);
		        	   Intent intent = new Intent(SeekingTripsList.this, SeekingOfferingRides.class);
		        	   startActivity(intent);
		           }
		       });
		}
		
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	void confirmAddTrip(final Trip addTrip)
	{
		
	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setTitle(getResources().getString(R.string.title_confirm));
	    builder.setMessage(getResources().getString(R.string.cna_confirmaddride));
	    /*final EditText input = new EditText(this); //THIS COULD BE USEFUL!
	    builder.setView(input);*/
	    builder.setPositiveButton(getResources().getString(R.string.cna_ok), new DialogInterface.OnClickListener()
	    {
	        @Override
	        public void onClick(DialogInterface dialog, int whichButton)
	        {
	        	Passenger newPassenger = new Passenger();
	        	newPassenger.setTripID(addTrip.getID());
	        	List<Passenger> passengers = ObjectManager.search(newPassenger);
	        	if(passengers.size() == addTrip.getMaxPassengers())
	        	{
	        		
	        		showDialog(getResources().getString(R.string.cna_ridepassengerlimit),true);
	        		return;
	        	}
	        	for(Passenger tempPassenger : passengers)
	        	{
	        		
	        		if(tempPassenger.getPassengerID() == ObjectManager.currentUser.getID())
	        		{
	        			showDialog(getResources().getString(R.string.cna_alreadyaddedride),true);
	        			return;
	        		}
	        	}
	        	newPassenger.setPassengerID(ObjectManager.currentUser.getID());
	        	newPassenger.setIsCheckedIn(2);
	        	newPassenger.setRideID(0);
	        	ObjectManager.add(newPassenger);
	        	
	        	ObjectManager.sendMessage(addTrip.getDriverID(),addTrip.getArrivalLocation(),3);
	        	
	        	showDialog(getResources().getString(R.string.cna_driverhasbeensentnotificaiton),false);
	        }
	    });
	    builder.setNegativeButton(getResources().getString(R.string.cna_cancel), new DialogInterface.OnClickListener()
	    {
	        @Override
	        public void onClick(DialogInterface dialog, int whichButton)
	        {
	        	//do stuff
	        	//showDialog("I still have to implement this :( - George",true);
	        	return;
	        }
	    });
	    builder.create().show();
	}
	
	@Override
	 public void onBackPressed()
	 {
		 Intent intent = new Intent(this, SeekingRideMenu.class);
		 startActivity(intent);
	 }
}
