package com.android.wheelshare160;

//http://stackoverflow.com/questions/5070830/populating-a-listview-using-arraylist
//Also, read the following:
//http://stackoverflow.com/questions/456211/activity-restart-on-rotation-android - LOL WUT

import java.util.ArrayList;
import java.util.List;
import com.wheelshare.objects.*;
import android.os.Bundle;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.AdapterView; 
import android.widget.AdapterView.OnItemClickListener;


public class ViewAcceptedRides extends Activity {
	
	static Trip currentTrip;
	static List<Trip> currentTripList = new ArrayList<Trip>();
	static List<Passenger> currentPassengerList = new ArrayList<Passenger>();
	
	List<User> currentUserList = new ArrayList<User>();
	public class TripListAdapter extends ArrayAdapter<Trip> {

	    private List<Trip> items;
	    private Context context;

	    public TripListAdapter(Context context, int textViewResourceId, List<Trip> items) {
	        super(context, textViewResourceId, items);
	        this.context = context;
	        this.items = items;
	    }

	    public View getView(int position, View convertView, ViewGroup parent) {
	        View view = convertView;
	        if (view == null) {
	            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            view = inflater.inflate(R.layout.trip_item, null);
	        }
	        //Log.i("wheelshare160","WHY IS THIS HAPPENING?" + String.valueOf(position) + " " + String.valueOf(items.size()) + " " + String.valueOf(currentUserList.size()));
	        Trip trip = items.get(position);
	        User user = currentUserList.get(position);
	        if (trip!= null) {
	            // My layout has only one TextView
	            TextView itemView = (TextView) view.findViewById(R.id.tripItemTitleView);
	            if (itemView != null) {
	                itemView.setText(trip.getArrivalLocation() + " on " + ObjectManager.getDate(trip.getDepartureDate()) +
	                				 " at " + ObjectManager.getTime(trip.getDepartureTime()));
	            }
	            
	            itemView = (TextView) view.findViewById(R.id.tripItemDepartureView);
	            if (itemView != null) {
	                itemView.setText("Offered by " + user.getFirstName() + " " + user.getLastName() + " from " + trip.getDepartureLocation());
	            }
	            
	            
	         }

	        return view;
	    }
	}

	private ListView tripListView;
	TripListAdapter tripListAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_trips_list);
		// Show the Up button in the action bar.
		setupActionBar();
    	
		tripListView =  (ListView) findViewById(R.id.tripListView);
		
    	Passenger queryPassenger = new Passenger();
    	queryPassenger.setPassengerID(ObjectManager.currentUser.getID());
    	currentPassengerList = ObjectManager.search(queryPassenger);
    	
    	currentTripList.clear();
    	for(Passenger passenger : currentPassengerList)
    	{
    		Trip queryTrip = new Trip();
    		queryTrip.setID(passenger.getTripID());
    		currentTripList.add(ObjectManager.search(queryTrip).get(0));
    	}
    	
    	currentUserList.clear();
    	User queryUser = new User();
    	for(int i = 0; i < currentTripList.size(); i++)
    	{
    		queryUser.setID(currentTripList.get(i).getDriverID());
    		currentUserList.add(ObjectManager.search(queryUser).get(0));
    	}
        
    	tripListAdapter = new TripListAdapter(this,android.R.layout.simple_list_item_1, currentTripList);
    	tripListView.setAdapter(tripListAdapter);
    	tripListView.setOnItemClickListener(new OnItemClickListener()
    	{
    		public void onItemClick(AdapterView<?> myAdapter, View myView, int position, long mylng)
    		{
    			Trip selectedTrip = (Trip)(tripListView.getItemAtPosition(position));
    			ViewAcceptedRide.currentPassenger = currentPassengerList.get(position);
    			ViewAcceptedRide.currentTrip = selectedTrip;
    			Intent intent = new Intent(ViewAcceptedRides.this, ViewAcceptedRide.class);
		        startActivity(intent);
    		}    		             
    	});
    	
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.objects_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	 public void onBackPressed()
	 {
		 Intent intent = new Intent(this, SeekingOfferingRides.class);
		 startActivity(intent);
	 }
}
