package com.android.wheelshare160;

//http://dharmendra4android.blogspot.com/2012/10/multi-selection-listview-example-in.html

import java.util.ArrayList;
import java.util.List;
import com.wheelshare.objects.*;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;

public class ViewOfferedTrip extends Activity {
	
	static Trip currentTrip;
	static List<Ride> currentRideList = new ArrayList<Ride>();
	List<User> currentUserList = new ArrayList<User>();
	
	MultiSelectionAdapter<Ride> rideAdapter;
	ListView offeredRideListView;
	Button offeredTripCreateButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_offered_trip);
		// Show the Up button in the action bar.
		setupActionBar();
		TextView view = (TextView) findViewById(R.id.offeredDepartureLocationTextView);
		
		
		view.setText(getResources().getString(R.string.cna_departure_location) + currentTrip.getDepartureLocation());
		view = (TextView) findViewById(R.id.offeredArrivalLocationTextView);
		
		
		view.setText(getResources().getString(R.string.cna_arrival_location) + currentTrip.getArrivalLocation());
		view = (TextView) findViewById(R.id.offeredDepartureDateTextView);
		
		
		view.setText(getResources().getString(R.string.cna_departure_date) + ObjectManager.getDate(currentTrip.getDepartureDate()));
		view = (TextView) findViewById(R.id.offeredDepartureTimeTextView);
		
		
		view.setText(getResources().getString(R.string.cna_departure_time) + ObjectManager.getTime(currentTrip.getDepartureTime()));
		view = (TextView) findViewById(R.id.offeredMaxPassengersTextView);
		
		
		view.setText(getResources().getString(R.string.cna_maximum_passengers) + Integer.toString(currentTrip.getMaxPassengers()));
		view = (TextView) findViewById(R.id.offeredCostTextView);
		
		
		view.setText(getResources().getString(R.string.cna_cost) + Integer.toString(currentTrip.getCost()));
		
		if(currentTrip.getRoundTrip() == 1)
		{
			view = (TextView) findViewById(R.id.offeredReturnDateTextView);
			
			
			view.setText(getResources().getString(R.string.cna_return_date) + ObjectManager.getDate(currentTrip.getReturnDate()));
			view = (TextView) findViewById(R.id.offeredReturnTimeTextView);
			
			
			view.setText(getResources().getString(R.string.cna_return_time) + ObjectManager.getTime(currentTrip.getReturnTime()));
		}
		else
		{
			view = (TextView) findViewById(R.id.offeredReturnDateTextView);
			
			view.setText(getResources().getString(R.string.cna_return_date) + getResources().getString(R.string.cna_notaroundtrip));
			view = (TextView) findViewById(R.id.offeredReturnTimeTextView);
			view.setText(getResources().getString(R.string.cna_return_time) + getResources().getString(R.string.cna_notaroundtrip));
		}
		
		if(currentTrip.getOneTime() == 1)
		{
			
			view = (TextView) findViewById(R.id.offeredWeekDaysTextView);
			view.setText(getResources().getString(R.string.cna_daysoftheweek) + currentTrip.getWeekDays());
		}
		else
		{
			
			view = (TextView) findViewById(R.id.offeredWeekDaysTextView);
			view.setText(getResources().getString(R.string.cna_daysoftheweek) + getResources().getString(R.string.cna_notacarpool));
		}
		
		
		
		Ride queryRide = new Ride();
		queryRide.setDepartureLocation(currentTrip.getDepartureLocation());
		queryRide.setArrivalLocation(currentTrip.getArrivalLocation());
		queryRide.setDepartureDate(currentTrip.getDepartureDate());
		
		currentRideList = ObjectManager.search(queryRide);
		
		for(Ride ride : currentRideList)
		{
			User user = new User();
			user.setID(ride.getRiderID());
			currentUserList.add(ObjectManager.search(user).get(0));
		}
		
		offeredRideListView = (ListView) findViewById(R.id.offeredTripRideListView);
		offeredTripCreateButton = (Button) findViewById(R.id.offeredTripCreateButton);
		rideAdapter = new MultiSelectionAdapter<Ride>(this, currentRideList)
		{
			@Override
			public View getView(final int position, View convertView, ViewGroup parent)
			{
				// TODO Auto-generated method stub
				if(convertView == null)
				{
					convertView = mInflater.inflate(R.layout.ride_item, null);
				}
				
				
				TextView tvTitle = (TextView) convertView.findViewById(R.id.checkableRideTextView);
				User user = currentUserList.get(position);
				tvTitle.setText(user.getFirstName() + " " + user.getLastName() + " - " + user.getUserName());
				
				
				CheckBox mCheckBox = (CheckBox) convertView.findViewById(R.id.checkableRideCheckBox);
				mCheckBox.setTag(position);
				mCheckBox.setChecked(mSparseBooleanArray.get(position));
				mCheckBox.setOnCheckedChangeListener(mCheckedChangeListener);
				return convertView;
			}
		};
		offeredRideListView.setAdapter(rideAdapter);
	}
	
	public void CreateTrip(View view)
	{
		//TODO do stuff
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setTitle(getResources().getString(R.string.title_confirm));
	    builder.setMessage(getResources().getString(R.string.cna_confirmofferride));
	    builder.setPositiveButton(getResources().getString(R.string.cna_ok), new DialogInterface.OnClickListener()
	    {
	        @Override
	        public void onClick(DialogInterface dialog, int whichButton)
	        {
	        	Trip addedTrip = ObjectManager.add(currentTrip);
	        	
	        	if(addedTrip != null)
	        	{
	        		
	        		String str = getResources().getString(R.string.cna_tripsuccessfullyposted);
	        		
	        		currentRideList = rideAdapter.getCheckedItems();
	        		
	        		if(currentRideList.size() != 0)
	        		{
	        			
	        			str += getResources().getString(R.string.cna_allselectedusers);
	        			for(Ride ride : currentRideList)
	        			{
	        				Passenger passenger = new Passenger();
	        				passenger.setTripID(addedTrip.getID());
	        				passenger.setPassengerID(ride.getRiderID());
	        				passenger.setRideID(ride.getID());
	        				passenger.setIsCheckedIn(3);
	        				
	        				ObjectManager.sendMessage(ride.getRiderID(),currentTrip.getArrivalLocation(),2);
	        				
	        				ObjectManager.add(passenger);
	        			}
	        		}
	        		showDialog(str,false);
	        	}
	        	
	        	else showDialog("getResources().getString(R.string.cna_tripunabletobeposted)",true);
	        }
	    });
	    builder.setNegativeButton(getResources().getString(R.string.cna_cancel), new DialogInterface.OnClickListener()
	    {
	        @Override
	        public void onClick(DialogInterface dialog, int whichButton)
	        {
	        	return;
	        }
	    });
	    builder.create().show();
	}
	
	void showDialog(final String message, boolean error)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(ViewOfferedTrip.this);
		if(error)
		{
			builder.setTitle(getResources().getString(R.string.title_error));
			builder.setMessage(message)
		       .setCancelable(false)
		       .setNeutralButton(getResources().getString(R.string.cna_ok), new DialogInterface.OnClickListener()
		       {
		           public void onClick(DialogInterface dialog, int id)
		           {
		        	   Log.e("wheelshare160", "ERROR: " + message);
		           }
		       });
		}
		else
		{
			builder.setTitle(getResources().getString(R.string.title_success));
			builder.setMessage(message)
		       .setCancelable(false)
		       .setNeutralButton(getResources().getString(R.string.cna_ok), new DialogInterface.OnClickListener()
		       {
		           public void onClick(DialogInterface dialog, int id)
		           {
		        	   Log.i("wheelshare160", "SUCCESS: " + message);
		        	   Intent intent = new Intent(ViewOfferedTrip.this, SeekingOfferingRides.class);
		               startActivity(intent);
		           }
		       });
		}
		
		AlertDialog alert = builder.create();
		alert.show();
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.objects_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	 public void onBackPressed()
	 {
		 Intent intent = new Intent(this, OfferingRideMenu.class);
		 startActivity(intent);
	 }
}
