package com.android.wheelshare160;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.android.wheelshare160.R;
import com.wheelshare.objects.ObjectManager;
import com.wheelshare.objects.Passenger;
import com.wheelshare.objects.Trip;
import com.wheelshare.objects.User;

//http://stackoverflow.com/questions/4384890/how-to-disable-an-android-button

public class ViewPassenger extends Activity {
	
	public static Passenger currentPassenger;
	public static Trip currentTrip;
	public static User currentUser;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_passenger);
		
		TextView viewPassengerNameView = (TextView) findViewById(R.id.viewPassengerNameView);
		viewPassengerNameView.setText(currentUser.getFirstName() + " " + currentUser.getLastName() + ",");
		TextView viewPassengerNickNameView = (TextView) findViewById(R.id.viewPassengerNickNameView);
		viewPassengerNickNameView.setText("Username: " + currentUser.getLastName());
		TextView viewPassengerPhoneView = (TextView) findViewById(R.id.viewPassengerPhoneView);
		viewPassengerPhoneView.setText("Phone Number: " + currentUser.getPhoneNumber());
		
		TextView viewPassengerStatusView = (TextView) findViewById(R.id.viewPassengerStatusView);
    	if(currentPassenger.getIsCheckedIn() == 1)
    	{
    		viewPassengerStatusView.setText(getResources().getString(R.string.cna_passengeralreadycheckedin));
    		viewPassengerStatusView.setBackgroundColor(Color.GREEN);
    	}
    	else if(currentPassenger.getIsCheckedIn() == 3)
    	{
    		viewPassengerStatusView.setText(getResources().getString(R.string.cna_passengeryettocheckin));
    		viewPassengerStatusView.setBackgroundColor(Color.BLACK);
    	}
    	else if(currentPassenger.getIsCheckedIn() == 2)
    	{
    		viewPassengerStatusView.setText(getResources().getString(R.string.cna_passengerawaitingconfirmation));
    		viewPassengerStatusView.setBackgroundColor(Color.GRAY);
    	}
    	else viewPassengerStatusView.setText(getResources().getString(R.string.cna_passengerreadytocheckin));
		
		Button acceptButton = (Button)findViewById(R.id.acceptPassengerButton);
		if(currentPassenger.getIsCheckedIn() != 2) acceptButton.setEnabled(false);
	}
	
	void showDialog(final String message, boolean error, final boolean action)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(ViewPassenger.this);
		if(error)
		{
			builder.setTitle(getResources().getString(R.string.title_error));
			builder.setMessage(message)
		       .setCancelable(false)
		       .setNeutralButton(getResources().getString(R.string.cna_ok), new DialogInterface.OnClickListener()
		       {
		           public void onClick(DialogInterface dialog, int id)
		           {
		        	   Log.e("wheelshare160", "ERROR: " + message);
		           }
		       });
		}
		else
		{
			builder.setTitle(getResources().getString(R.string.title_success));
			builder.setMessage(message)
		       .setCancelable(false)
		       .setNeutralButton(getResources().getString(R.string.cna_ok), new DialogInterface.OnClickListener()
		       {
		           public void onClick(DialogInterface dialog, int id)
		           {
		        	   Log.i("wheelshare160", "SUCCESS: " + message);
		        	   if(action)
		        	   {
		        		   Intent intent = new Intent(ViewPassenger.this, ViewTrip.class);
		       	        	startActivity(intent);
		        	   }
		           }
		       });
		}
		builder.create().show();
	}
	
	public void AcceptPassenger(View view)
	{		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setTitle(getResources().getString(R.string.title_confirm));
	    builder.setMessage(getResources().getString(R.string.cna_confirmaddingusertotrip));
	    builder.setPositiveButton(getResources().getString(R.string.cna_ok), new DialogInterface.OnClickListener()
	    {
	        @Override
	        public void onClick(DialogInterface dialog, int whichButton)
	        {
	        	currentPassenger.setIsCheckedIn(4);
				
				ObjectManager.sendMessage(currentPassenger.getPassengerID(),currentTrip.getArrivalLocation(),5);
	        	
	        	ObjectManager.update(currentPassenger);
	        	showDialog(getResources().getString(R.string.cna_passengeralreadyaddedtotrip),false,true);
	        	
	        }
	    });
	    builder.setNegativeButton(getResources().getString(R.string.cna_cancel), new DialogInterface.OnClickListener()
	    {
	        @Override
	        public void onClick(DialogInterface dialog, int whichButton)
	        {
	        	return;
	        }
	    });
	    builder.create().show();
	}
    
	public void RemovePassenger(View view)
	{
		//TODO delete the passenger object from the datastore
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setTitle(getResources().getString(R.string.title_confirm));
	    builder.setMessage(getResources().getString(R.string.cna_confirmdeleteuserfromtrip));
	    builder.setPositiveButton(getResources().getString(R.string.cna_ok), new DialogInterface.OnClickListener()
	    {
	        @Override
	        public void onClick(DialogInterface dialog, int whichButton)
	        {
				ObjectManager.sendMessage(currentPassenger.getPassengerID(),currentTrip.getArrivalLocation(),4);
	        	
	        	ObjectManager.delete(currentPassenger);
	        	showDialog(getResources().getString(R.string.cna_passengerdeletedfromtrip),false,true);
	        }
	    });
	    builder.setNegativeButton(getResources().getString(R.string.cna_cancel), new DialogInterface.OnClickListener()
	    {
	        @Override
	        public void onClick(DialogInterface dialog, int whichButton)
	        {
	        	return;
	        }
	    });
	    builder.create().show();
	}
	
	@Override
	 public void onBackPressed()
	 {
		 Intent intent = new Intent(this, ViewTrip.class);
		 startActivity(intent);
	 }
}
