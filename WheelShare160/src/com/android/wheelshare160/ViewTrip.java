package com.android.wheelshare160;

//http://stackoverflow.com/questions/5070830/populating-a-listview-using-arraylist
//Also, read the following:
//http://stackoverflow.com/questions/456211/activity-restart-on-rotation-android - LOL WUT

import java.util.ArrayList;
import java.util.List;
import com.wheelshare.objects.*;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.widget.AdapterView; 
import android.widget.AdapterView.OnItemClickListener;


public class ViewTrip extends Activity {
	
	static Trip currentTrip;
	static boolean searchResult;
	//another thing I learned: lists cannot be instantiated because they are interfaces!
	static List<Passenger> currentPassengerList = new ArrayList<Passenger>();
	List<User> currentUserList = new ArrayList<User>();
	public class PassengerListAdapter extends ArrayAdapter<Passenger> {

	    private List<Passenger> items;
	    private Context context;

	    public PassengerListAdapter(Context context, int textViewResourceId, List<Passenger> items) {
	        super(context, textViewResourceId, items);
	        this.context = context;
	        this.items = items;
	    }

	    public View getView(int position, View convertView, ViewGroup parent) {
	        View view = convertView;
	        if (view == null) {
	            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            view = inflater.inflate(R.layout.passenger_item, null);
	        }
	        Passenger passenger = items.get(position);
	        User user = currentUserList.get(position);
	        if (passenger!= null) {
	            // My layout has only one TextView
	        	TextView itemView = (TextView) view.findViewById(R.id.passengerItemView);
	        	if(passenger.getIsCheckedIn() == 1)
	        		itemView.setBackgroundColor(Color.GREEN);
	        	else if(passenger.getIsCheckedIn() == 2)
	        		itemView.setBackgroundColor(Color.GRAY);
	        	else if(passenger.getIsCheckedIn() == 3)
	        		itemView.setBackgroundColor(Color.BLACK);
	        	//else if(passenger.getIsCheckedIn() == 4)
	        		//itemView.setBackgroundColor(Color.BLACK);
	            if (itemView != null) {
	            	itemView.setText(user.getFirstName() + " " + user.getLastName() + " - " + user.getUserName());
	            }
	            else Log.e("wheelshare160","WHY IS THIS NULL?");
	            
	         }

	        return view;
	    }
	}

	private ListView passengerListView;
	PassengerListAdapter passengerListAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_trip);
		// Show the Up button in the action bar.
		setupActionBar();
		
		TextView view = (TextView) findViewById(R.id.viewTripDepartureLocationTextView);
		view.setText(getResources().getString(R.string.cna_departure_location) + currentTrip.getDepartureLocation());
		view = (TextView) findViewById(R.id.viewTripArrivalLocationTextView);
		view.setText(getResources().getString(R.string.cna_arrival_location) + currentTrip.getArrivalLocation());
		view = (TextView) findViewById(R.id.viewTripDepartureDateTextView);
		view.setText(getResources().getString(R.string.cna_departure_date) + ObjectManager.getDate(currentTrip.getDepartureDate()));
		view = (TextView) findViewById(R.id.viewTripDepartureTimeTextView);
		view.setText(getResources().getString(R.string.cna_departure_time) + ObjectManager.getTime(currentTrip.getDepartureTime()));
		view = (TextView) findViewById(R.id.viewTripMaxPassengersTextView);
		view.setText(getResources().getString(R.string.cna_maximum_passengers) + Integer.toString(currentTrip.getMaxPassengers()));
		view = (TextView) findViewById(R.id.viewTripCostTextView);
		view.setText(getResources().getString(R.string.cna_cost) + Integer.toString(currentTrip.getCost()));
		
		if(currentTrip.getRoundTrip() == 1)
		{
			view = (TextView) findViewById(R.id.viewTripReturnDateTextView);
			view.setText(getResources().getString(R.string.cna_return_date) + ObjectManager.getDate(currentTrip.getReturnDate()));
			view = (TextView) findViewById(R.id.viewTripReturnTimeTextView);
			view.setText(getResources().getString(R.string.cna_return_time) + ObjectManager.getTime(currentTrip.getReturnTime()));
		}
		else
		{
			view = (TextView) findViewById(R.id.viewTripReturnDateTextView);
			view.setText(getResources().getString(R.string.cna_return_date) + getResources().getString(R.string.cna_notaroundtrip));
			view = (TextView) findViewById(R.id.viewTripReturnTimeTextView);
			view.setText(getResources().getString(R.string.cna_return_time) + getResources().getString(R.string.cna_notaroundtrip));
		}
		
		if(currentTrip.getOneTime() == 1)
		{
			view = (TextView) findViewById(R.id.viewTripWeekDaysTextView);
			view.setText(getResources().getString(R.string.cna_daysoftheweek) + currentTrip.getWeekDays());
		}
		else
		{
			view = (TextView) findViewById(R.id.viewTripWeekDaysTextView);
			view.setText(getResources().getString(R.string.cna_daysoftheweek) + getResources().getString(R.string.cna_notacarpool));
		}
    	
		passengerListView =  (ListView) findViewById(R.id.passengerListView);
		
		Passenger queryPassenger = new Passenger();
		queryPassenger.setTripID(currentTrip.getID());
		
    	currentPassengerList = ObjectManager.search(queryPassenger);
    	
    	currentUserList.clear();
    	User queryUser = new User();
    	for(int i = 0; i < currentPassengerList.size(); i++)
    	{
    		queryUser.setID(currentPassengerList.get(i).getPassengerID());
    		currentUserList.add(ObjectManager.search(queryUser).get(0));
    	}
        
    	passengerListAdapter = new PassengerListAdapter(this,android.R.layout.simple_list_item_1, currentPassengerList);
    	passengerListView.setAdapter(passengerListAdapter);
    	passengerListView.setOnItemClickListener(new OnItemClickListener()
    	{
    		public void onItemClick(AdapterView<?> myAdapter, View myView, int position, long mylng)
    		{
    			ViewPassenger.currentUser = currentUserList.get(position);
    			ViewPassenger.currentPassenger = currentPassengerList.get(position);
    			ViewPassenger.currentTrip = currentTrip;
    			Intent intent = new Intent(ViewTrip.this, ViewPassenger.class);
    	        startActivity(intent);
    			
    		}                 
    	});
    	
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.objects_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed()
	{
		Intent intent = new Intent(this, ViewTrips.class);
		if(searchResult) intent = new Intent(this, SeekingTripsList.class);
		startActivity(intent);
	}
}
