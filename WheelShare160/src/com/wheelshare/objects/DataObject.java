package com.wheelshare.objects;

public class DataObject {
	protected int ID;	
	public int getID() {
		return ID;
	}
	public void setID(int ID) {
		this.ID = ID;
	}
	public String [] toStringArray(){
		String [] returnString = {String.valueOf(ID)};
		return returnString;
	}
}
