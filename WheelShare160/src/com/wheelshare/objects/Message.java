package com.wheelshare.objects;

public class Message extends DataObject{
	int toID;
	int fromID;
	String text;
	int isRead;
	int messageID;

	public Message()
	{
		//do stuff
	}
	
	public int getMessageID() {
		return messageID;
	}

	public void setMessageID(int messageID) {
		this.messageID = messageID;
	}
	
	public int getToID() {
		return toID;
	}



	public void setToID(int toID) {
		this.toID = toID;
	}



	public int getFromID() {
		return fromID;
	}



	public void setFromID(int fromID) {
		this.fromID = fromID;
	}



	public String getText() {
		return text;
	}



	public void setText(String text) {
		this.text = text;
	}



	public int getIsRead() {
		return isRead;
	}



	public void setIsRead(int isRead) {
		this.isRead = isRead;
	}
	
	public String toString(){
		return "Rating [ID=" + getID() +
			", toID=" + toID +
			", fromID=" + fromID +
			", text=" + text + 
			", isRead=" + isRead + 
			", messageID=" + messageID + 
			"]";
	}
	
	public String[] toStringArray(){
		String [] stringArray = {
			"ID", String.valueOf(getID()),
			"toID", String.valueOf(getToID()),
			"fromID", String.valueOf(getFromID()),
			"text", String.valueOf(getText()),
			"isRead", String.valueOf(getIsRead()),
			"messageID", String.valueOf(getMessageID())
			};
		return stringArray;
	}
}
