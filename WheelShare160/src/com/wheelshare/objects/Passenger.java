package com.wheelshare.objects;

public class Passenger extends DataObject{
	private int tripID;
	private int passengerID;	//holds the user ID of the passenger
	/* 0 for unspecified, 1 for checked in, 2 for pending acceptance
	 * from driver, 3 for pending acceptance from passenger, 4 for
	 * added to ride, but not checked in.
	 */
	private int isCheckedIn;
	private int rideID; //if the passenger was created via a ride request
	
	public Passenger()
	{
		//do stuff
	}
	
	public int getRideID() {
		return rideID;
	}

	public void setRideID(int rideID) {
		this.rideID = rideID;
	}
	
	public int getIsCheckedIn() {
		return isCheckedIn;
	}
	public void setIsCheckedIn(int isCheckedIn) {
		this.isCheckedIn = isCheckedIn;
	}
	public int getPassengerID() {
		return passengerID;
	}
	public void setPassengerID(int passengerID) {
		this.passengerID = passengerID;
	}
	public int getTripID() {
		return tripID;
	}
	public void setTripID(int tripID) {
		this.tripID = tripID;
	}

	public String toString(){
		return "Passenger [ID=" + getID() +
			", tripID=" + tripID +
			", passengerID=" + passengerID + 
			", rideID=" + rideID + 
			", isCheckedIn=" + isCheckedIn + "]";
	}
	
	public String[] toStringArray(){
		String [] stringArray = {
			"ID", String.valueOf(getID()),
			"tripID", String.valueOf(getTripID()),
			"passengerID", String.valueOf(getPassengerID()),
			"rideID", String.valueOf(getRideID()),
			"isCheckedIn", String.valueOf(getIsCheckedIn())};
		return stringArray;
	}
}
