package com.wheelshare.objects;

import android.util.Log;

public class Rating extends DataObject{
	int driverID;
	int raterID;
	int rating;	//should be a value between 0-5
	
	public Rating()
	{
		//do stuff
	}
	
	public int getDriverID() {
		return driverID;
	}
	public void setDriverID(int driverID) {
		this.driverID = driverID;
	}
	public int getRaterID() {
		return raterID;
	}
	public void setRaterID(int userID) {
		this.raterID = userID;
	}
	public int getRating() {
		return rating;
	}
	public boolean setRating(int rating) {
		if(rating <= 5)
		{
			this.rating = rating;
			return true;
		}
		Log.e("wheelshare160","ERROR: Rating can only be between 0 and 5.");
		return false;
	}
	
	public String toString(){
		return "Rating [ID=" + getID() +
			", driverID=" + driverID +
			", raterID=" + raterID +
			", rating=" + rating + "]";
	}
	
	public String[] toStringArray(){
		String [] stringArray = {
			"ID", String.valueOf(getID()),
			"driverID", String.valueOf(getDriverID()),
			"raterID", String.valueOf(getRaterID()),
			"rating", String.valueOf(getRating())};
		return stringArray;
	}
}
