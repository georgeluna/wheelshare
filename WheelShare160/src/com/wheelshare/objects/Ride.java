package com.wheelshare.objects;

public class Ride extends DataObject{
	private int riderID;
	private int departureTime;
	
	/* 1 would be midnight, between 0-3, 2 would be early morning, 4-7,
	 * 3 would be morning, 8-11, 4 would be afternoon, 12-15, 5 would be
	 * evening, 16-19, 6 would be night, 20-23, 0 is unspecified.
	 */	
	private int departureTimeOfDay;
	private int departureDate;
	private String departureLocation;
	private String arrivalLocation;
	
	private int cost; //the maximum amount of money that a user is willing to spend on a trip
	
	private int roundTrip; //1 for false, 2 for true, 0 for unspecified
		private int returnTime;
		
		/* 1 would be midnight, between 0-3, 2 would be early morning, 4-7,
		 * 3 would be morning, 8-11, 4 would be afternoon, 12-15, 5 would be
		 * evening, 16-19, 6 would be night, 20-23, 0 is unspecified.
		 */	
		private int returnTimeOfDay;
		private int returnDate;
	
	//checks whether this is a one time
	//thing, or if it's a daily thing.
	private int oneTime; //1 for false, 2 for true, 0 for unspecified
		//a bit string of length 7 that holds
		//which days of the week this trip
		//applies to.
		private String weekDays = ""; //an empty or null string indicates unspecified
		
	public Ride()
	{
		//do stuff
	}
	
	public int getRiderID() {
		return riderID;
	}

	public void setRiderID(int riderID) {
		this.riderID = riderID;
	}

	public int getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(int departureTime) {
		this.departureTime = departureTime;
	}

	public int getDepartureTimeOfDay() {
		return departureTimeOfDay;
	}

	public void setDepartureTimeOfDay(int departureTimeOfDay) {
		this.departureTimeOfDay = departureTimeOfDay;
	}

	public int getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(int departureDate) {
		this.departureDate = departureDate;
	}

	public String getDepartureLocation() {
		return departureLocation;
	}

	public void setDepartureLocation(String departureLocation) {
		this.departureLocation = departureLocation;
	}

	public String getArrivalLocation() {
		return arrivalLocation;
	}

	public void setArrivalLocation(String arrivalLocation) {
		this.arrivalLocation = arrivalLocation;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getRoundTrip() {
		return roundTrip;
	}

	public void setRoundTrip(int roundTrip) {
		this.roundTrip = roundTrip;
	}

	public int getReturnTime() {
		return returnTime;
	}

	public void setReturnTime(int returnTime) {
		this.returnTime = returnTime;
	}

	public int getReturnTimeOfDay() {
		return returnTimeOfDay;
	}

	public void setReturnTimeOfDay(int returnTimeOfDay) {
		this.returnTimeOfDay = returnTimeOfDay;
	}

	public int getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(int returnDate) {
		this.returnDate = returnDate;
	}

	public int getOneTime() {
		return oneTime;
	}

	public void setOneTime(int oneTime) {
		this.oneTime = oneTime;
	}

	public String getWeekDays() {
		return weekDays;
	}

	public void setWeekDays(String weekDays) {
		this.weekDays = weekDays;
	}
	
	public String toString(){
		return "Ride [ID=" + getID() +
			", riderID=" + riderID +
			", departureTime=" + departureTime +
			", departureTimeOfDay=" + departureTimeOfDay +
			", departureDate=" + departureDate +
			", departureLocation=" + departureLocation +
			", arrivalLocation=" + arrivalLocation +
			", cost=" + cost +
			", roundTrip=" + roundTrip +
			", returnTime=" + returnTime +
			", returnTimeOfDay=" + returnTimeOfDay +
			", returnDate=" + returnDate +
			", oneTime=" + oneTime +
			", weekDays=" + weekDays + "]";
	}
	
	public String [] toStringArray(){
		String [] stringArray = {
			"ID", String.valueOf(getID()),
			"riderID", String.valueOf(riderID),
			"departureTime", String.valueOf(departureTime),
			"departureTimeOfDay", String.valueOf(departureTimeOfDay),
			"departureDate", String.valueOf(departureDate),
			"departureLocation", departureLocation,
			"arrivalLocation", arrivalLocation,
			"cost", String.valueOf(cost),
			"roundTrip", String.valueOf(roundTrip),
			"returnTime", String.valueOf(returnTime),
			"returnTimeOfDay", String.valueOf(returnTimeOfDay),
			"returnDate", String.valueOf(returnDate),
			"oneTime", String.valueOf(oneTime),
			"weekDays", weekDays,};
		return stringArray;
	}
}
