package com.wheelshare.objects;

public class User extends DataObject{
	private String userName;
	private int gender; //1 for male, 2 for female, 0 for unspecified
	private String email;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private String password;
	private String regID;
	
	public String getRegID() {
		return regID;
	}

	public void setRegID(String regID) {
		this.regID = regID;
	}

	private long joinDate;
	
	public User()
	{
		//do stuff
	}
	
	public long getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(long joinDate) {
		this.joinDate = joinDate;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String toString(){
		return "User [ID=" + getID() +
			", userName=" + userName +
			", gender=" + gender +
			", email=" + email +
			", firstName=" + firstName +
			", lastName=" + lastName +
			",phoneNumber=" + phoneNumber +
			", password=" + password +
			", joinDate=" + joinDate + 
			", regID=" + regID + "]";
	}
	
	public String[] toStringArray(){
		String [] stringArray = {
			"ID", String.valueOf(getID()),
			"userName", userName,
			"gender",String.valueOf(gender),
			"email", email,
			"firstName", firstName,
			"lastName", lastName,
			"phoneNumber", phoneNumber,
			"password", password,
			"joinDate", String.valueOf(joinDate),
			"regID", String.valueOf(regID)};
		return stringArray;
	}

}
