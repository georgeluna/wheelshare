import webapp2

import datetime
import cgi
import urllib
import webapp2

from google.appengine.ext import db
from google.appengine.api import users


class MainPage(webapp2.RequestHandler):

    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.write('Hello, webapp2 World!')

application = webapp2.WSGIApplication([
    ('/', MainPage),
], debug=True)
